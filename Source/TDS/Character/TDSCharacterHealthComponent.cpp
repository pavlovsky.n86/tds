// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterHealthComponent.h"

void UTDSCharacterHealthComponent::ChangeHealthValue(float ChngeValue)
{
	float CurrentDamage = ChngeValue * CoefDamage;
		
	if (Shield > 0.f && ChngeValue < 0.0f)
	{
		ChangeShieldValue(ChngeValue);
		if (Shield < 0.f)
		{
			//FX
			UE_LOG(LogTemp, Warning, TEXT("UTDSCharacterHealthComponent::ChangeCurrentHealth - Shield <0"));

		}
	}
	else
	{
		Super::ChangeHealthValue(ChngeValue);
	}
}

float UTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float ChngeValue)
{
	Shield += ChngeValue;

	if (Shield > 100.f)
	{
		Shield = 100.f;
	}
	else
	{
		if (Shield < 0.f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	OnShieldChange.Broadcast(Shield, ChngeValue);
}

void UTDSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld() && Health > 0.f)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if (tmp > 100.f)
	{
		Shield = 100.f;
		if (GetWorld())
		{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
